# MOOC-shell-bash
Lien du MOOC : https://www.fun-mooc.fr/fr/cours/maitriser-le-shell-bash/
## Contexte
Utilisateur de GNU/Linux, je souhaitais approfondir mes connaissances en bash acquises en DUT et dans le cours de système d'exploitation.
## Compétences acquises

Ce cours m'a appris à :
- Décrire par des commandes des tâches courantes à effectuer sur mon ordinateur,
- Utiliser une console pour administrer et interagir avec mon ordinateur,
- Configurer mon environnement,
- Enchaîner des commandes pour effectuer des traitements spécifiques,
- Ecrire des scripts shell pour automatiser des travaux,
- Lire des scripts shell et comprendre leur fonctionnement,
- Corriger des erreurs dans des scripts existants.